#include "Health.h"
#include <QFont>
#include<QFontDatabase>
#include <QDebug>

Health::Health(QGraphicsItem *parent)
    : QGraphicsTextItem(parent)
{
    healthNb_ = 10;
    int id = QFontDatabase::addApplicationFont(":/fonts/Joystix Monospace.ttf");
    QFontDatabase::applicationFontFamilies(id);

    setPlainText(QString::number(healthNb_)+QString(" HP"));
    setDefaultTextColor(Qt::darkGreen);

    setFont(QFont("Joystix Monospace",20));


}

Health::~Health()
{

}

void Health::increaseH() {
    healthNb_++;
    if (healthNb_ >= 5 ) {
        setDefaultTextColor(Qt::darkGreen);
    }
    setPlainText(QString::number(healthNb_)+QString(" HP"));

}

int Health::getHealthNb()
{
    return healthNb_;
}

void Health::setHealthNb(int healthNb)
{
    healthNb_ = healthNb;
    if (healthNb_< 5 ) {
        setDefaultTextColor(Qt::red);
    }
    setPlainText(QString::number(healthNb_)+QString(" HP"));
}
