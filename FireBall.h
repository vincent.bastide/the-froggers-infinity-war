#ifndef FIREBALL_H
#define FIREBALL_H

//#include <QGraphicsRectItem>
//#include <QPainter>
#include <QGraphicsPixmapItem>
#include "RectPlatform.h"

#define SCENE_WIDTH 5000
#define SCENE_HEIGHT 400

#define SCENE_GROUND_HEIGHT 368

class FireBall : public QObject, public QGraphicsPixmapItem {

    Q_OBJECT

public :
    FireBall(bool direction, QGraphicsItem * parent = 0);
    ~FireBall();
    /*void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
      QRectF boundingRect() const;*/
    bool handleCollisions();

private slots :
    void move();

private:
    QTimer * fireballTimer_;
    bool direction_;
    QPixmap * fireball_image_;



};


#endif // FIREBALL_H
