#ifndef SCENE_H
#define SCENE_H

#include <QDebug>
#include <QObject>
#include <QGraphicsScene>
#include <QGraphicsRectItem>
#include <QPainter>
#include <QPixmap>


#include "Health.h"
#include "Player.h"
#include "RectPlatform.h"
#include "Platform.h"
#include "ground.h"
#define SCENE_WIDTH 5000
#define SCENE_HEIGHT 400

#define SCENE_GROUND_HEIGHT 368

class Scene : public QGraphicsScene
{
    Q_OBJECT
public:
    Scene(QObject *parent = nullptr);

    //draw
    void drawBackground(QPainter * painter, const QRectF &rect);

    //Getters & Setters
    Player * getPlayer(){ return player_;};
    Health * getHealth(){ return health_;};

    // Collisions
    QGraphicsItem* collidingPlatforms();
    bool handleCollisionWithRectItems();


private:
    // Draw
    QPixmap * sprite_image_;

    // State
    bool is_running_;

    // Items
    Player * player_;
    Health * health_;
    QList < RectPlatform *> rectplatforms_;
    Ground * ground;

};

#endif // SCENE_H
