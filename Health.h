#ifndef HEALTH_H
#define HEALTH_H

#include <QGraphicsTextItem>

class Health : public QGraphicsTextItem{

 public:
    Health(QGraphicsItem * parent = 0);
    ~Health();
    int getHealthNb();
    void setHealthNb( int healthNb) ;
    void increaseH();

 private :
    int healthNb_;


};

#endif // HEALTH_H
