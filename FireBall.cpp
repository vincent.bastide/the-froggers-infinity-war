#include "FireBall.h"
#include <QTimer>

//#include <QPen>

#include <QDebug>


FireBall::FireBall(bool is_left, QGraphicsItem * parent)
    : QObject(), QGraphicsPixmapItem(parent), direction_(is_left)
{
    setFlag(ItemClipsToShape);

    if (direction_ == true){
        setPixmap(QPixmap(":/sprites/leftFireball.png"));}


    else {
        setPixmap(QPixmap(":/sprites/rightFireball.png"));
        }

    fireballTimer_ = new QTimer();
    fireballTimer_->connect(fireballTimer_, SIGNAL(timeout()), this, SLOT(move()));
    fireballTimer_->start(50);


}

FireBall::~FireBall()
{

    qDebug() << "on delete la firebal" <<  this->x() << endl;
}

bool FireBall::handleCollisions()
{
    QList<QGraphicsItem*> items =  collidingItems();
    for (int i = 0 ; i < items.size(); i++) {
        if ( typeid (*(items[i])) == typeid (RectPlatform)){

            delete this;
            qDebug() <<  "BOUM" << endl;
            return true;
        }
    }
    return false;
}

/*void FireBall::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
        painter->drawPixmap(
                    QRectF(0, 0, 100, 100),
                    *fireball_image_,
                    QRectF(0, 32, 32, 32));
        Q_UNUSED(option);
        Q_UNUSED(widget);

}
QRectF FireBall::boundingRect() const { return QRectF(0, 0, 32, 32); }
*/

void FireBall::move()
{
    if (x() > SCENE_WIDTH - 10 || x() <= 0){
        qDebug() << x() << endl;
        delete this;
        return;
    }

    if(handleCollisions()) return;

    if (direction_ == true) {
        moveBy(-17, 0);
    }
    else {
        moveBy(17, 0);
    }

}


